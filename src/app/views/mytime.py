from django.shortcuts import render

from app.services.mytime import MyTimeService

mytime_service = MyTimeService()


def mytime(request):
    times = mytime_service.get_all()
    return render(request, 'mytime.html', context={'times': times})
