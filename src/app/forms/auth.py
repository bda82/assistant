from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email: forms.Field = forms.EmailField()
    password1: forms.Field = forms.PasswordInput()
    password2: forms.Field = forms.PasswordInput()

    fields = ['username', 'email', 'password1', 'password2']
