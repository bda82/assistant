# Generated by Django 4.0.6 on 2022-07-29 09:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MyTime',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField()),
                ('url', models.TextField()),
                ('started', models.DateTimeField()),
                ('stopped', models.DateTimeField()),
                ('active', models.BooleanField()),
                ('distance', models.IntegerField(default=0)),
            ],
        ),
    ]
