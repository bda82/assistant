from app.models.mytime import MyTime
from app.services.base_service import BaseService


class MyTimeService(BaseService):
    model = MyTime
