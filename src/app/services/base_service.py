

class BaseService:
    model = None

    def get_all(self):
        return self.model.objects.all().order_by('started')

    def get(self, pk):
        try:
            return self.model.objects.get(pk)
        except Exception as e:
            return None
