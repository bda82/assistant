from django.db import models


class MyTime(models.Model):
    title = models.TextField(default='')
    description = models.TextField(default='')
    url = models.TextField(default='')
    started = models.DateTimeField()
    stopped = models.DateTimeField()
    active = models.BooleanField(default=False)
    distance = models.IntegerField(default=0)

    objects = models.Manager()

    def __str__(self):
        return f'[{self.id}] {self.title}'
